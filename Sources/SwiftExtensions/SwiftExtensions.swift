import Foundation
import UIKit

public extension String {

    static var forcedLanguage: String?

    // MARK: - Localization

    var localized: String {
        if let forcedLanguage = String.forcedLanguage {
            return self.localized(for: forcedLanguage)
        }

        return NSLocalizedString(self, comment: "")
    }

        fileprivate func localized(for language: String) -> String {
            let path = Bundle.main.path(forResource: language, ofType: "lproj")!
            let bundle = Bundle(path: path)!
            let localizedString = NSLocalizedString(self, bundle: bundle, comment: "")

        return localizedString
    }

}

public extension UIView {
    func makeCornerRadius(_ radius: CGFloat = 7) {
        layer.cornerRadius = radius
    }
}


public extension UIView {
 
 func anchor (top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat, enableInsets: Bool) {
     
        var topInset = CGFloat(0)
        var bottomInset = CGFloat(0)
         
        if #available(iOS 11, *), enableInsets {
            let insets = self.safeAreaInsets
            topInset = insets.top
            bottomInset = insets.bottom
            
            print("Top: \(topInset)")
            print("bottom: \(bottomInset)")
        }
             
        translatesAutoresizingMaskIntoConstraints = false
             
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop+topInset).isActive = true
        }
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom-bottomInset).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
    
    }
 
}
