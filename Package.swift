// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "SwiftExtensions",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "SwiftExtensions",
            targets: ["SwiftExtensions"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "SwiftExtensions",
            dependencies: [],
            path: "./Sources/SwiftExtensions"),
        .testTarget(
            name: "SwiftExtensionsTests",
            dependencies: ["SwiftExtensions"],
            path: "./Tests/SwiftExtensionsTests"),
    ]
)
